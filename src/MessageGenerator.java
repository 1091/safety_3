import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MessageGenerator{
	Set<char[]> set;
	Random rand;
	int Max = 30;
	public MessageGenerator(){
		set  = new HashSet<char[]>();
		rand = new Random();
	}
	public String newRndTestingString() {
		char[] workCH = new char[Max];
		do
			for (int i = 0; i < Max; i++)
				workCH[i] = (char)(rand.nextInt(93)+33);
		while (set.contains(workCH));
		set.add(workCH);
		return String.valueOf(workCH);
	}
}