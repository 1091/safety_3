import java.security.MessageDigest;

public class SHA256 {
	public String CreateNewHash(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();
	        hash = getMyHash(hash);
	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) 
	            	hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString();
	    } 
	    catch(Exception ex){
	    	throw new RuntimeException(ex);
	    }
	}
	public byte[] getMyHash(byte[] chIN){
		char [] Ch  = toBinary(chIN).toCharArray();
		char [] Ch2 = new char[56];
		int j=55;
		for (int i=64; i>14; i--)
			Ch2[j--]=Ch[i];
		for (int i=0; i<15; i++)
			Ch2[i]='0';		
        return fromBinary(String.valueOf(Ch2));
	}
	private String toBinary( byte[] bytes )	{
	    StringBuilder sb = new StringBuilder(bytes.length * Byte.SIZE);
	    for( int i = 0; i < Byte.SIZE * bytes.length; i++ )
	        sb.append((bytes[i / Byte.SIZE] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
	    return sb.toString();
	}
	private byte[] fromBinary( String s ){
	    int sLen = s.length();
	    byte[] toReturn = new byte[(sLen + Byte.SIZE - 1) / Byte.SIZE];
	    char c;
	    for( int i = 0; i < sLen; i++ )
	        if( (c = s.charAt(i)) == '1' )
	            toReturn[i / Byte.SIZE] = (byte) (toReturn[i / Byte.SIZE] | (0x80 >>> (i % Byte.SIZE)));
	        else if ( c != '0' )
	            throw new IllegalArgumentException();
	    return toReturn;
	}
}
